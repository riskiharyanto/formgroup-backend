var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'formgroup-backend'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/formgroup-backend-development'
  },

  test: {
    root: rootPath,
    app: {
      name: 'formgroup-backend'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/formgroup-backend-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'formgroup-backend'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/formgroup-backend-production'
  }
};

module.exports = config[env];
