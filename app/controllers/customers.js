const saltRounds = 10;
var cors = require('cors');
var bcrypt = require('bcrypt');
var express = require('express'),
  app = express();
  router = express.Router(),
  mongoose = require('mongoose'),
  Customers = mongoose.model('Customers');

module.exports = function (app) {
  app.use(cors());
  app.use('/v1', router);
};
app.set('superSecret', 'UserLogin');

// =====================
//-------------

// User-Add
router.post('/customers', function(req, res, next){
	bcrypt.hash(req.body.password, saltRounds, function(err, hash){
		if (err){
			console.log(err);
		} else {
			var input = new Customers ({ 	name: req.body.name,
											password: hash,
											address: req.body.address,
											telp: req.body.telp
										});
			input.save(function(err){
				if(err){
					console.log(err)
				} else {
					res.status(200).json({status:'Success'});
				};
			});
		};
	});       
});
//-------------

// User-Update
router.post('/update/customers/:_id', function(req, res, next){
	var data = Customers.find({_id:req.params._id});
	data.exec(function(err, result){
		if(err){
			console.log(err);
		} else {
			if(result == null){
				res.status(404).json({status:'Not Found'});
			} else {
				var dataold = {_id:req.params._id};
				var datanew = {$set:{	name: req.body.name,
										address: req.body.adderss,
										telp: req.body.telp
									}};
				var options = {};
				Customers.update(dataold, datanew, options, callback);
				function callback(err){
					if(err){
						console.log(err);
					} else{
						res.status(200).json({status:'Success'});
					};
				};									
			};
		};
	});
});
//-------------

// User-Delete
router.delete('/customers/:_id', function(req, res, next){
	var data = Customers.remove({_id:req.params._id});
	data.exec(function(err){
		if(err){
			console.log(err);
		} else {
			res.status(200).json({status:'Success'});
		};
	});
});
//-------------

// User-Login
router.post('/login', function(req, res, next){
	var data = Customers.findOne({name:req.body.name});
	data.exec(function(err, result){
		if (err){s
			console.log(err);
		} else {
			if (result == null){
				res.status(404).json({status:'Name Not Found'});
			} else {
				var password_hash = result.password;
				bcrypt.compare(req.body.password, password_hash, function(err, compare){
	              	if (err){
	                	console.log(err);
	              	} else {
	                	if (compare == true){
	                  		res.status(200).json({status:'Login Success'});
	                	} else {
	                  		res.status(404).json({status:'Password Wrong'});
	                	};
	              	};
	            });
			};
		};
	});
});
//-------------

// User-Show
router.get('/customers', function(req, res, next){
	var data = Customers.find({});
	data.exec(function(err, result){
		if(err){
			console.log(err);
		} else {
			res.status(200).json(result);
		};
	});
});
//-------------
// =====================